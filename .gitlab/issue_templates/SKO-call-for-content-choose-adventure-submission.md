This issue template is designed to capture your proposal for a **"Choose Your Own Adventure" breakout session** at GitLab Sales Kickoff (SKO) as a part of the [Call for Content process](/handbook/sales/training/SKO/SKO-planning/#sko-call-for-content). Title this issue with `SKO Choose Adventure Content Submission: [Session Title]`. Sessions are 60 minutes each. 

<details><summary><b>Instructions: Click to expand</b></summary>

1. Fill out the below issue in full. (Incomplete submissions may not be considered.) 
1. Once the Field Enablement team has completed their review of content submissions, they will respond in this issue with the status and next-steps.
</details>

## [Enter Course Title Here] 

### Session Content

##### Session 1-2 Sentence Description 
<!-- 180 characters max --> 
[Answer here]

##### Session Longer Description
<!-- 500 characters max --> 
[Answer here]

### Goals and Objectives 

##### Goal
<!-- What is the end goal of this training for the audience/what’s in it for the audience? -->
[Answer here]

##### SKO Objectives Alignment
<!-- Explain how this course aligns with the [SKO objectives](https://about.gitlab.com/handbook/sales/training/SKO/#sales-kickoff-sko-overview) to MOTIVATE, CELEBRATE and ENABLE the GitLab Field team. -->
[Answer here]

### Course Details

##### Audience
<!-- Confirm this session applies to all Field team members and is not role-specific. If you are suggesting a role-specific breakout topic, see the [role-based breakout issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/new?issuable_template=SKO-call-for-content-role-based-submission) --> 
- [ ] Yes

##### Content Creator 
<!-- If you have a suggestion on which team member(s) could help create the content for this session, please provide that information here. -->
[Enter name(s) here]

##### Presenters
<!-- If you have a suggestion on which team member(s) could help deliver the content for this session, please provide that information here. -->
[Enter name(s) here]

##### Proposed Content Delivery
* [ ] Presentation-only (theater-style seating, stage, projection)
* [ ] Laptops-out session (classroom-style seating, power, projection)
* [ ] Discussion (flexible space with moveable seating, option for breakout sessions)

##### Proposed Format
<!-- How do you envision the format of this session – speaking-only, speaking + Q&A, speaking + workshop, guided workshop, etc. --> 
[Answer here]


----
*Do not edit below
/label ~"field enablement" ~"FE priority::1" ~"FE status::triage" ~"field events" ~"sko" ~"sko content submission" ~"sko content::triage"
/assign @kshirazi @pdaliparthi


