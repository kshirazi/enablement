**First Day of Enrollment:**

* (Date TBD)

**Last Day of Enrollment:**

* (Date TBD)

**Welcome Call**: 

* (Date TBD)

**Start Date:**

* (Date TBD)

**Level Up Onboarding Learning Paths:**

- Enterprise Strategic Account Leader - [Enterprise Sales: What every SAL needs to know (30-60-90 days)](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/enterprise-sales-sal-30-60-90-days) 
- Commercial Account Executive - [Commercial Sales - Onboarding Journey](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/commercial-sales-onboarding-journey)
- Solutions Architect - [Solutions Architect: 30-60-90 Onboarding Journey](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/solutions-architect-30-60-90-onboarding-journey)
- Customer Success Managers
    - [ Strategic Customer Success Manager (CSM) Onboarding](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/technical-account-manager-tam-onboarding) 
    - [Growth CSM and Scale CSE Onboarding](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/scale-and-mid-touch-tam-onboarding)
- Inside Sales - [Inside Sales: Onboarding Learner Journey](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/inside-sales-onboarding-learner-journey)
- Sales Development - [Tanuki Tech - Level 1](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/tanuki-tech-level-1)
- Channel - [Channel Sales Onboarding Journey](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/channel-sales-on-boarding)
- All Other Roles (Field Ops) - [Sales Quick Start](https://levelup.gitlab.com/learn/course/sales-quick-start)

**Class Roster, Mock Call Groups & Grade Report:** 

* (Link TBD)

**Pre-Class Survey**

* [Pre Workshop Survey Responses](https://docs.google.com/forms/d/1QEM28wCroVYLIN-UMJr0gXpFamOTnc4aZnFLM8d6MSA/edit#responses)

**Post-Class Survey**

* [Post Workshop Survey Responses](https://docs.google.com/forms/d/1MuXRKieYh86cffByFl6JCY2HiZ7A5vlWQb17tZalhqE/edit#responses)


## SQS Checklist:
* [ ] Confirm SQS start date
* [ ] Attendee spreadsheet created
* [ ] Create SQS cohort Slack channel
* [ ] Add attendees to SQS Slack channel
* [ ] Send Welcome Call calendar invite
* [ ] Engage PMMs and SMEs for SQS attendance/presentation 
* [ ] Create SQS live session notes document
* [ ] Send calendar invites for SMEs presenting an SQS session
* [ ] Send calendar invites for PMMs facilitating mock calls

## SQS Agenda:

| DATE | START TIME | END TIME | ACTIVITY | SME ASSIGNED |
| ------ | ------ | ------ | ------ | ------ |
| Oct 11, 2022 | 10:00a ET | 11:20a ET | Essential Questions Exercise | @jblevins608  |
| Oct 12, 2022 | 10:00a ET | 11:20a ET | Value Card Exercise | @jblevins608  |
| Oct 12, 2022 | 11:30a ET | 12:20p ET | Discovery Question Exercise | @jblevins608  |
| Oct 13, 2022 | 09:30a ET | 10:50a ET | Differentiator Exercise | @jblevins608  |
| Oct 14, 2022 | 10:00a ET | 11:20a ET | MEDDPPICC & Breakout Call Prep | @jblevins608  |
| Oct 17, 2022 | 09:30a ET | 09:50a ET | Field Security | @gitlab-com/gl-security/security-assurance/field-security-team    |
| Oct 17, 2022 | 10:00a ET | 10:50a ET | Intro to Competition |   |
| Oct 17, 2022 | 11:00a ET | 11:50a ET | Alliances |   |
| Oct 18, 2022 | 10:00a ET | 10:50a ET  | Discussion: Professional Services |   |
| Oct 18, 2022 | 11:00a ET | 11:50a ET | Discovery Call 1 | @jblevins608  + Mock Customers  |
| Oct 19, 2022 | 10:00a ET | 10:50a ET  | Legal / Deal Desk |  |
| Oct 19, 2022 | 11:00a ET | 11:50a ET | Discovery Call 2 | @jblevins608  + Mock Customers |
| Oct 20, 2022 | 10:00a ET | 10:50a ET | Channels |    |
| Oct 20, 2022 | 11:00a ET | 11:50a ET | Discovery Call 3 | @jblevins608  + Mock Customers |


cc: @jblevins608 @kshirazi @monicaj @pdaliparthi @emelier @azaglul @cenache @cttran @mzimmers @StefiPirvu

/label ~"sales onboarding" ~"field enablement"
/label ~"FE status::triage"
/assign @jblevins608
