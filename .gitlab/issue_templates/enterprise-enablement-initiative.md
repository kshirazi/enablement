This issue template is for the ENT Enablement program manager and designed to capture a new training and enablement initiative for Enterprise Enablement. Title this issue with `Enablement Request: [Topic]`.

## Background 
<!-- Give background on the enablement request. Including: what is it, why does it matter (what problem is it trying to solve), why now, and what's the impact on the business -->

[Answer here]

**What problem is this trying to solve?**

[Answer here]

**What is the impact on the business?**

[Answer here]

**Who are the key stakeholders and geos:**

[Answer here]

**How will we measure success?**

## Dependencies
What teams or SMEs are needed to create and collaborate on this enablement? 
What needs to happen for it be successful?
Are there any key dates that would block this enablement or that we should keep in mind?

## Deliverables
What type of initiative or program will be used to support this?
- [ ] - Net-new, high-impact quarterly training item
- [ ] - High-effort, high-impact ongoing program
- [ ] - Just-in-time enablement products and services ( we do 1-2 per quarter in ENT)

## Workback plan
Includes key dates for delivering new content, and rollout. Usually aligns to GTM process (#1500).
- [ ] - Item 1, Date 1
- [ ] - Item 1, Date 1
- [ ] - Item 1, Date 1

**What is the urgency level of this request?**
- [ ] - Urgent, immediate action needed
- [ ] - Medium urgency, action preferred this quarter
- [ ] - Not urgent, no specific timeline

----
*Do not edit below
/label ~"field enablement" ~"FE status::wip" ~"FE: ENT Sales"
/assign @emelier 

