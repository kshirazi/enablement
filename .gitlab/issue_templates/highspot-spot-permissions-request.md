This issue template is for the Highspot DRI or Spot Owner to grant additional access to users within Highspot. Title this issue with `Highspot Spot Permissions Request - [Your Name]`.

## Background 
<!-- Give background on the Highspot access request. Including: what you need to accomplish, why now, and what's the impact on the business -->

[Answer here]

**What Spot do you need to access? For example: Company Pitch Templates**

[Answer here]

**What level of access do you need?**

- [ ] - View (See all content in a Spot)
- [ ] - Edit (Be able to add Items and Templates to a Spot)
- [ ] - Own / Co-Own (Manage a Spot as the Owner)

**What is the urgency level of this request?**
- [ ] - Urgent, immediate action needed
- [ ] - Medium urgency, action preferred this quarter
- [ ] - Not urgent, no specific timeline

----
*Do not edit below
/label ~"field enablement" ~"FE status::wip" ~"FE priority::new request" ~"Highspot" 
/assign @abryson1
