This issue template is designed to capture feedback and needs for partner enablement. Title this issue with  `Partner Enablement Request: [Topic]`. Please note, while we will try our best to include your request in our Partner Enablement Roadmap as soon as possible, the scope of the request will determine it’s priority level. 


## Background 
<!-- Provide details about the enablement request that you are interested in making to the field organization or channel partners, including context on urgency and impact to the field, and expected outcomes/calls-to-action. Include as much detail as possible. -->
[Answer here]

### Request Info
+ Target completion date: 
+ DRI(s): 
+ Related issue(s) and/or Handbook page(s): 

### Request Info
<!-- Choose an option in each of the categories below. Delete all unselected options. This information is mandatory and will be used to generate the priority status on the roadmap -->
1. **Audience**
   - [ ] All Partners (All Partner Types and Regions)
   - [ ] All SELECT Partners
   - [ ] All OPEN Partners
   - [ ] Alliances
   - [ ] EMEA Only
   - [ ] AMER Only 
   - [ ] APAC Only
   - [ ] Public Sector Only
   - [ ] Global
   - [ ] Internal Channel Team
   - [ ] Internal Alliances Team
   - [ ] Internal Field Sales Team 
2. **Topic**
   - [ ] Technology Specific 
   - [ ] Operational Specific 
   - [ ] Soft Skill (Sales Strategy, Account Mapping, etc.)
   - [ ] System/Program Change or Update
   - [ ] Other [please type in answer]
1. **Type**
   - [ ] On-demand Course/curriculum including video examples via EdCast/GitLab Learn
   - [ ] On-demand Course/Curriculum text based via EdCast/ GitLab Learn
   - [ ] On-demand /Curriculum technical/hands-on assessment via EdCast/GitLab Learn
   - [ ] On-demand One Pager- Soft Copy PDF -available via highspot
   - [ ] On-demand One Pager- Hard Copy- requires printing 
   - [ ] Handbook Page
   - [ ] Other [please type in answer]
1. **Please provide details on request**
[Answer here]
1. **Please provide business case for request and link to any OKR this request is attached to**
[Answer here]

### Draft Copy
<!-- If you have an idea of what you'd like the asset  to say (i.e. a slide deck that was delivered but needs socialization) please input it here. Not required, but helpful for the Partner Enablement team if you already have something in mind. -->
[Answer here]





After submitting this request you'll be able to track your request on the [Partner Enablement Request Board.](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1645038?label_name[]=FE%3Apartner%20enablement&label_name[]=field%20enablement)


----
*Do not edit below
/label ~"Channel Enablement" ~"Channel" ~"Partner Enablement" ~"field enablement" ~"FE Partner Enablement" 
/assign @bfraser
