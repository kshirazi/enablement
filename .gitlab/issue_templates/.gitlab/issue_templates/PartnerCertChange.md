This issue template is to capture necessary changes to Certification and Accreditation records when a partner learner changes organizations. Please ensure that all actions are completed before updating certification records.  
 
 
 Has the individual partner user requested this change?   
   - [ ] Yes
   - [ ] No- if no, then we need written confirmation from the learner that they are requesting records to be updated. 

What is the learner's OLD email address?

What is the learner's NEW email address?

Has the learner updated their Credly profile?
  - [ ] Yes
 - [ ] No- if no, then the partner needs to first update their Credly profile. 

What certifications need to be migrated over?

Once records have been verified, the following actions will take place:

   - [ ] @denniszeissner to update Certification Tracking Dashboard
   - [ ] @bfraser to update Thought Industries
   - [ ] PHD to update Impartner Records
   - [ ] PHD to confirm all records have been updated 



  ----
*Do not edit below
/label ~"Channel Enablement" ~"Channel" ~"Partner Enablement" ~"field enablement" ~"FE Partner Enablement" 
/assign @bfraser  @denniszeissner  @ecollett 
