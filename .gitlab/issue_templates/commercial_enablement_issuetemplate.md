This issue template is for the COMM Enablement program manager and designed to capture a new training and enablement initiative for Commercial Sales Enablement.

## Background
<!-- Give background on the enablement request. Including: what is it, why does it matter (what problem is it trying to solve), why now, and what's the impact on the business -->

**Learning Objectives:** 

Can this be shared with partners?  
- [ ] Yes
- [ ] No

**Success Criteria/Measurement**



**Field Functional Competency alignment** 
- [ ] Customer Focus
- [ ] Solution Focus
- [ ] Operational Excellence


## Workback Plan





















*Do not edit below
/label ~"field enablement" ~"FE status::wip" ~"FE: Commercial Sales"
/assign @kshirazi
