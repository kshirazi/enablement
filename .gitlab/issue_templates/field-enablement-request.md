This issue template is designed to capture a new training and enablement request. Title this issue with `Enablement Request: [Topic]`.

## Background 
<!-- Please provide background on your enablement request by answering the questions below. Note that ALL questions must be answered prior to Field Enablement triaging this request. Include as much detail as possible. -->

**Please summarize your request.**

[Answer here]

**What problem is this trying to solve?**

[Answer here]

**What is the impact on the business?**

[Answer here]

**Who is the intended audience?**
- [ ] - SALs 
- [ ] - AEs 
- [ ] - SDRs 
- [ ] - SAs 
- [ ] - TAMs
- [ ] - Sales management 
- [ ] - CS management 
- [ ] - Channel 
- [ ] - Alliances 

**What is the target segment?**
- [ ] - ENT 
- [ ] - COMM
- [ ] - CS 
- [ ] - Marketing (SDR) 
- [ ] - CAMs
- [ ] - ABDMs 
- [ ] - Partners


**Is this request related to an existing training/enablement initiative or is it net new?**

[Answer here]

**Who are the primary requestors?**

[Answer here]

**What is the urgency level of this request?**
- [ ] - Urgent, immediate action needed
- [ ] - Medium urgency, action preferred this quarter
- [ ] - Not urgent, no specific timeline

### Please confirm that you have done the following prior to submitting this issue request: 
- [ ] - Read the intake process on the Field Enablement Handbook 
- [ ] - Answered all questions above in detail 

----
*Do not edit below
/label ~"field enablement" ~"FE priority::new request" ~"FE status::triage"
/assign @bfraser  @kshirazi
