### Email Format 

TO: sales-all, SDR
CC: Farnoosh 

SUBJ:

Another month, another amazing GitLab release.   

1. GitLab is continuing to invest in a COMPLETE DevOps Platform for the entire software development lifecycle that helps our customers:
   * Increase operational efficiencies
   * Deliver better products faster
   * Reduce security and compliance risk
1. [Our latest release] helps teams: 
   1. a
   1. b
   1. c
   1. Much more...
1. **ADD** 3-4 updates from GitLab. Usually content (blog, remote work, etc.) or big announcements (i.e. partner program launch)

Please contact us if you would like to schedule time with a Solution Architect to review GitLab **VERSION NUMBER**, Upcoming Releases, or the [**NEXT VERSION NUMBER** Kickoff](https://about.gitlab.com/direction/kickoff/). 

**ADD** Copy/paste release information from the [GitLab Releases page](https://gitlab.com/gitlab-org/gitlab/-/releases) below. Ensure the appropriate tags are included for features that are .com and self-managed-only: 

**Sections**
1. Ultimate
1. Premium
1. Core 


----
## Process
1. Follow #release-post channel closely and use information from the [GitLab Releases page](https://about.gitlab.com/releases/) to complete email once posted. 
1. Send email to sales-all and SDR distro lists 
1. Copy email into a template in Outreach. 
   1. Store it under the "Sales 2020 GitLab Feature Releases" campaign. 
   1. Make sure you set sharing settings to, "Others can see and use it." 
1. Send sales a notification in #sales Slack channel:
   1. The release post is live (share link to blog) 
   1. Check your email for a version you can easily forward to customers, or it's [here] (link to template) in Outreach for anyone who wants to access it there. 
   1. Remember to share the release post on social.
1. Cross-post #sales message to #sdr_global Slack channel

---

/label ~"field communications" ~"field enablement" ~"FE priority::1" ~"FE status::wip"
/assign @shannonthompson 
