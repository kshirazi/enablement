This issue template is designed to capture your proposal for a **role-based breakout session** at GitLab Sales Kickoff (SKO) as a part of the [Call for Content process](/handbook/sales/training/SKO/SKO-planning/#sko-call-for-content). Title this issue with `SKO Role-Based Content Submission: [Session Title]`. Sessions are 60 minutes each. 

<details><summary><b>Instructions: Click to expand</b></summary>

1. Fill out the below issue in full. (Incomplete submissions may not be considered.) 
1. Once the Field Enablement team has completed their review of content submissions, they will respond in this issue with the status and next-steps.
</details>


## [Enter Course Title Here] 

### Goals and Objectives 

##### Goal
<!-- What is the end goal of this training for the audience/what’s in it for the audience? -->
[Answer here]

##### After completing this session, the team member will be able to:
<!-- Identify prospects who... / Effectively position the messaging to... / Use the tool to ... -->
1. [Answer here]
1. [Answer here]
1. [Answer here]

##### SKO Objectives Alignment
<!-- Explain how this course aligns with the [SKO objectives](https://about.gitlab.com/handbook/sales/training/SKO/#sales-kickoff-sko-overview) to MOTIVATE, CELEBRATE and ENABLE the GitLab Field team. -->
[Answer here]

### Course Details

##### Audience
<!-- Delete selections that are not chosen. --> 

- [ ] All Field
- [ ] ENT Sales
- [ ] COMM Sales 
- [ ] Channel
- [ ] Alliances
- [ ] Solutions Architects 
- [ ] Technical Account Managers
- [ ] Professional Services 

##### Content Creator 
[Enter name(s) here]

##### Presenters
[Enter name(s) here]

##### Executive Sponsor
[Enter name(s) here]

##### Proposed Content Delivery
* [ ] Presentation-only (theater-style seating, stage, projection)
* [ ] Laptops-out session (classroom-style seating, power, projection)
* [ ] Discussion (flexible space with moveable seating, option for breakout sessions)

##### Proposed Format
<!-- How do you envision the format of this session – speaking-only, speaking + Q&A, speaking + workshop, guided workshop, etc. --> 

[Answer here]

### Session Content
<!-- Fill out the below course outline providing as much detail as possible. --> 

##### Session 1-2 Sentence Description 
<!-- 180 characters max --> 

[Answer here]

##### Session Longer Description
<!-- 500 characters max --> 

[Answer here]

##### Learning Objective No. 1: [Enter Title Here]
<!-- Provide supporting messages for this learning objective. --> 

1. [Detail 1]
1. [Detail 2]
1. [Detail 3]

##### Learning Objective No. 2: [Enter Title Here]
<!-- Provide supporting messages for this learning objective. --> 

1. [Detail 1]
1. [Detail 2]
1. [Detail 3]

##### Learning Objective No. 3: [Enter Title Here]
<!-- Provide supporting messages for this learning objective. --> 

1. [Detail 1]
1. [Detail 2]
1. [Detail 3]

##### Wrap-Up
<!-- What additional resources, next steps and/or call to action(s) will you give the audience? --> 

[Answer here]

----
*Do not edit below
/label ~"field enablement" ~"FE priority::1" ~"FE status::triage" ~"field events" ~"sko" ~"sko content submission" ~"sko content::triage"
/assign @kshirazi @pdaliparthi
