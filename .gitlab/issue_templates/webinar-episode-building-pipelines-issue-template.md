<!-- Creating the issue
1. Set the title in the format: "BP001 2022-05-04 Working Title"
`BP<3 digit episode number> <Date of Episode> <Title of Episode>`
2. Set the Issue Due Date as the target episode recording date
3. Set the Milestone appropriately
-->

## Episode Data
**Topic Blurb:**

**Guest Speaker:**  

**Working Preso Link:** 

**Archive Direct Link:**

## Episode Notes







## Do not edit below
/assign @bfraser @sbrightwell
/label ~CSA-Activity ~Channel ~"Channel Enablement" ~"FE:partner enablement" ~"field enablement"
/epic gitlab-com/sales-team/field-operations&117 